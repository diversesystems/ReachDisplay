package me.dewgs.reachdisplay.commands;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import me.dewgs.reachdisplay.ReachDisplayMod;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;

public class ReachDisplayModCommand extends CommandBase
{
    private ReachDisplayMod mod;
    
    public ReachDisplayModCommand(final ReachDisplayMod mod) {
        this.mod = mod;
    }
    
    public String getCommandName() {
        return "reachdisplaymod";
    }
    
    public String getCommandUsage(final ICommandSender p_71518_1_) {
        return "/reachdisplaymod";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        this.mod.setShowGui();
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean canCommandSenderUseCommand(final ICommandSender p_71519_1_) {
        return true;
    }
}
