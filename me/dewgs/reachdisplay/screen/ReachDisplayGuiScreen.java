package me.dewgs.reachdisplay.screen;

import net.minecraft.client.gui.GuiButton;
import me.dewgs.reachdisplay.color.Color;
import me.dewgs.reachdisplay.ReachDisplayMod;
import net.minecraft.client.gui.GuiScreen;

public class ReachDisplayGuiScreen extends GuiScreen
{
    private ReachDisplayMod mod;
    private boolean dragging;
    private int lastMouseX;
    private int lastMouseY;
    private int lastAddX;
    private int lastAddY;
    
    public ReachDisplayGuiScreen(final ReachDisplayMod mod) {
        this.mod = mod;
    }
    
    public void initGui() {
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 6 + 96, "Color: " + Color.values()[this.mod.getCurrentColorIndex()].getName()));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 6 + 72, "Enabled: " + (this.mod.isEnabled() ? "Yes" : "No")));
    }
    
    protected void actionPerformed(final GuiButton guiButton) {
        if (guiButton.enabled) {
            switch (guiButton.id) {
                case 0: {
                    this.mod.setCurrentColorIndex(this.mod.getCurrentColorIndex() + 1);
                    if (this.mod.getCurrentColorIndex() == Color.values().length) {
                        this.mod.setCurrentColorIndex(0);
                    }
                    final String colorName = Color.values()[this.mod.getCurrentColorIndex()].getName();
                    guiButton.displayString = "Color: " + colorName;
                    break;
                }
                case 1: {
                    this.mod.setEnabled(!this.mod.isEnabled());
                    guiButton.displayString = "Enabled: " + (this.mod.isEnabled() ? "Yes" : "No");
                    break;
                }
            }
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        if (state == 0) {
            this.dragging = false;
        }
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int lastButtonClicked, final long timeSinceMouseclick) {
        if (!this.dragging && this.mod.isEnabled() && mouseX > this.mod.getMinX() && mouseX < this.mod.getMaxX() && mouseY > this.mod.getMinY() && mouseY < this.mod.getMaxY()) {
            this.dragging = true;
            this.lastMouseX = mouseX;
            this.lastMouseY = mouseY;
            this.lastAddX = this.mod.getAddX();
            this.lastAddY = this.mod.getAddY();
        }
        else if (this.dragging) {
            this.mod.setAddX(this.lastAddX + (mouseX - this.lastMouseX));
            this.mod.setAddY(this.lastAddY + (mouseY - this.lastMouseY));
        }
    }
    
    public void onGuiClosed() {
        this.mod.saveConfig();
    }
}
