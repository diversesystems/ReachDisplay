package me.dewgs.reachdisplay;

import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import net.minecraft.client.gui.GuiScreen;
import java.io.IOException;
import net.minecraft.client.gui.Gui;
import me.dewgs.reachdisplay.color.Color;
import me.dewgs.reachdisplay.screen.ReachDisplayGuiScreen;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.math.Vec3d;
import java.text.DecimalFormat;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraft.command.ICommand;
import me.dewgs.reachdisplay.commands.ReachDisplayModCommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "reachdisplaymod", name = "Reach Display Mod", version = "1.0")
public class ReachDisplayMod
{
    private Minecraft mc;
    private String rangeText;
    private boolean enabled;
    private boolean showGui;
    private int minX;
    private int minY;
    private int maxX;
    private int maxY;
    private int addX;
    private int addY;
    private int currentColorIndex;
    private long lastAttack;
    
    public ReachDisplayMod() {
        this.rangeText = "Hasn't attacked";
        this.enabled = true;
        this.addX = 0;
        this.addY = 0;
    }
    
    @Mod.EventHandler
    public void onFMLInitialization(final FMLInitializationEvent event) {
        ClientCommandHandler.instance.registerCommand((ICommand)new ReachDisplayModCommand(this));
        FMLCommonHandler.instance().bus().register((Object)this);
        MinecraftForge.EVENT_BUS.register((Object)this);
        this.mc = Minecraft.getMinecraft();
        this.loadConfig();
    }
    
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onAttack(final AttackEntityEvent event) {
        if (this.mc.objectMouseOver != null && this.mc.objectMouseOver.typeOfHit == RayTraceResult.Type.ENTITY && this.mc.objectMouseOver.entityHit.getEntityId() == event.getTarget().getEntityId()) {
            final Vec3d vec3 = this.mc.getRenderViewEntity().getPositionEyes(1.0f);
            final double range = this.mc.objectMouseOver.hitVec.distanceTo(vec3);
            this.rangeText = new DecimalFormat(".##").format(range) + " blocks";
        }
        else {
            this.rangeText = "Not on target?";
        }
        this.lastAttack = System.currentTimeMillis();
    }
    
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onRenderTick(final TickEvent.RenderTickEvent event) {
        if ((this.mc.currentScreen == null || this.mc.currentScreen instanceof ReachDisplayGuiScreen) && this.mc.theWorld != null && this.enabled) {
            if (System.currentTimeMillis() - this.lastAttack > 2000L) {
                this.rangeText = "Hasn't attacked";
            }
            final int minX = this.addX;
            final int minY = this.addY;
            final int maxX = this.addX + 4 + this.mc.fontRendererObj.getStringWidth(this.rangeText);
            final int maxY = this.addY + 4 + this.mc.fontRendererObj.FONT_HEIGHT;
            final Color textColor = Color.values()[this.currentColorIndex];
            int textHex = textColor.getHex();
            if (textColor == Color.RAINBOW) {
                textHex = java.awt.Color.HSBtoRGB((float)(System.currentTimeMillis() % 1000L) / 1000.0f, 0.8f, 0.8f);
            }
            Gui.drawRect(minX, minY, maxX, maxY, 1342177280);
            this.mc.fontRendererObj.drawString(this.rangeText, minX + 2, minY + 2, textHex);
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
            if (this.mc.currentScreen != null) {
                try {
                    this.mc.currentScreen.handleInput();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (this.showGui) {
            this.mc.displayGuiScreen((GuiScreen)new ReachDisplayGuiScreen(this));
            this.showGui = false;
        }
    }
    
    public void setShowGui() {
        this.showGui = true;
    }
    
    public void setAddX(final int addX) {
        this.addX = addX;
    }
    
    public void setAddY(final int addY) {
        this.addY = addY;
    }
    
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
    
    public void setCurrentColorIndex(final int currentColorIndex) {
        this.currentColorIndex = currentColorIndex;
    }
    
    public boolean isEnabled() {
        return this.enabled;
    }
    
    public int getCurrentColorIndex() {
        return this.currentColorIndex;
    }
    
    public int getMinX() {
        return this.minX;
    }
    
    public int getMinY() {
        return this.minY;
    }
    
    public int getMaxX() {
        return this.maxX;
    }
    
    public int getMaxY() {
        return this.maxY;
    }
    
    public int getAddX() {
        return this.addX;
    }
    
    public int getAddY() {
        return this.addY;
    }
    
    public void saveConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "ReachDisplayMod", "values.cfg");
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final FileWriter writer = new FileWriter(file, false);
            writer.write(this.addX + "\n" + this.addY + "\n" + this.enabled + "\n" + this.currentColorIndex);
            writer.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    private void loadConfig() {
        try {
            final File file = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "ReachDisplayMod", "values.cfg");
            if (!file.exists()) {
                return;
            }
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            int i = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                switch (++i) {
                    case 1: {
                        this.addX = Integer.parseInt(line);
                        continue;
                    }
                    case 2: {
                        this.addY = Integer.parseInt(line);
                        continue;
                    }
                    case 3: {
                        this.enabled = Boolean.parseBoolean(line);
                        continue;
                    }
                    case 4: {
                        this.currentColorIndex = Integer.parseInt(line);
                        continue;
                    }
                }
            }
            reader.close();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
