package me.dewgs.reachdisplay.color;

public enum Color
{
    WHITE("White", 553648127), 
    RED("Red", 16711680), 
    GREEN("Green", 32768), 
    LIME("Lime", 65280), 
    PURPLE("Purple", 8388736), 
    AQUA("Aqua", 65535), 
    PINK("Pink", 16711935), 
    RAINBOW("Rainbow", -1);
    
    private String name;
    private int hex;
    
    private Color(final String name, final int hex) {
        this.name = name;
        this.hex = hex;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getHex() {
        return this.hex;
    }
}
